Pluto Command Line Interface (pluto-cli) is a free and open-source command line interface
for [libpluto][libpluto].

Documentation
-------------

For while, it does not exist. Sorry about that :(.

Compilation
-----------

###*nix
Run make PLUTO_LIB_PATH=/path/to/pluto-libraries PLUTO_HEADER_PATH=/path/to/pluto-headers.

By default, PLUTO_LIB_PATH and PLUTO_HEADER_PATH are ../libpluto.

Dependencies
------------

- libpluto
- SQLite3

[libpluto]: https://bitbucket.org/filipeaneto/libpluto/overview
