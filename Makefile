CFLAGS=-Wall

PLUTO_LIB_PATH=../libpluto
PLUTO_HEADER_PATH=../libpluto

all: pluto

.PHONY: clean

pluto: main.o $(PLUTO_LIB_PATH)/libpluto.a
	g++ $(CFLAGS) -L$(PLUTO_LIB_PATH) main.o -Wl,-Bstatic -lpluto -Wl,-Bdynamic -lsqlite3 -o pluto

main.o: main.cpp $(PLUTO_HEADER_PATH)/pluto.h
	g++ $(CFLAGS) -c main.cpp -I$(PLUTO_HEADER_PATH) -o main.o

clean:
	rm -f pluto main.o
