/**
 * This file is part of Pluto Personal Finance Command Line Interface.
 *
 * Pluto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pluto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pluto.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <set>
#include <map>

#include "pluto.h"

#define MAX_REPORT_SIZE 10240

// FIXME use more c++98 than c99

typedef void (*Command)(void);

using namespace std;

void exit();
void open();
void close();

void status();

void account();
void account_create();
void account_delete();
void account_rename();
void account_unset();
void account_list();
void account_set();

void report();
void income();
void spending();

void update();

void update_spending();
void update_income();
void update_account();
void update_date();
void update_tag();
void update_description();
void update_quantity();
void update_value();

void remove();

void alloc();
void dealloc();

void search();
void select();

void display();

void help();

static bool running = true;
static stringstream line;

static string dbname;
static string accountname;

static map<string, Command> commands;
static map<string, Command> account_commands;
static map<string, Command> update_commands;

static map<string, pl_filter_t> filters;
static map<string, pl_report_t> reports;

static pl_item_list_t item_list = {0, 0, 0};
static set<size_t> selected;
static map<size_t, size_t> repeated;

static char error[MAX_ERROR_MESSAGE_SIZE + 1];

int main(int argc, char *argv[])
{
    map<string, Command>::iterator cmd;

    string str;

    commands["exit"] = exit;
    commands["x"] = exit;
    commands["open"] = open;
    commands["o"] = open;
    commands["close"] = close;
    commands["c"] = close;
    commands["status"] = status;
    commands["st"] = status;
    commands["account"] = account;
    commands["ac"] = account;
    commands["report"] = report;
    commands["r"] = report;
    commands["income"] = income;
    commands["in"] = income;
    commands["spending"] = spending;
    commands["sp"] = spending;
    commands["allocate"] = alloc;
    commands["alloc"] = alloc;
    commands["deallocate"] = dealloc;
    commands["de"] = dealloc;
    commands["search"] = search;
    commands["s"] = search;
    commands["display"] = display;
    commands["d"] = display;
    commands["select"] = select;
    commands["sel"] = select;
    commands["update"] = update;
    commands["upd"] = update;
    commands["remove"] = remove;
    commands["rem"] = remove;
    commands["help"] = help;
    commands["h"] = help;

    account_commands["create"] = account_create;
    account_commands["c"] = account_create;
    account_commands["delete"] = account_delete;
    account_commands["d"] = account_delete;
    account_commands["rename"] = account_rename;
    account_commands["r"] = account_rename;
    account_commands["unset"] = account_unset;
    account_commands["u"] = account_unset;
    account_commands["list"] = account_list;
    account_commands["l"] = account_list;
    account_commands["set"] = account_set;
    account_commands["s"] = account_set;

    update_commands["account"] = update_account;
    update_commands["ac"] = update_account;
    update_commands["income"] = update_income;
    update_commands["in"] = update_income;
    update_commands["spending"] = update_spending;
    update_commands["sp"] = update_spending;
    update_commands["date"] = update_date;
    update_commands["d"] = update_date;
    update_commands["tag"] = update_tag;
    update_commands["t"] = update_tag;
    update_commands["description"] = update_description;
    update_commands["desc"] = update_description;
    update_commands["quantity"] = update_quantity;
    update_commands["qty"] = update_quantity;
    update_commands["value"] = update_value;
    update_commands["v"] = update_value;

    filters["most recent"] = PL_FILTER_MOST_RECENT;
    filters["recent"] = PL_FILTER_MOST_RECENT;

    reports["balance"] = PL_REPORT_BALANCE;
    reports["balance by tag"] = PL_REPORT_BALANCE_BY_TAG;
    reports["btag"] = PL_REPORT_BALANCE_BY_TAG;

    if (argc > 1)
    {
        line << "\"" << argv[1] << "\" ";
        open();
    }

    if (argc > 2)
    {
        line << "\"" << argv[2] << "\" ";
        account_set();
    }

    cout << "Welcome to pluto!\n";

    while (running)
    {
        cout << ">> ";

        str.clear();
        getline(cin, str);

        if (( cin.rdstate() & istream::eofbit ) != 0)
        {
            cout << '\n';
            running = false;
            continue;
        }

        if (str.empty())
            continue;

        line.clear();
        line.str(str);

        str.clear();
        line >> skipws >> str;
        cmd = commands.find(str);

        if (cmd == commands.end())
            cout << "Invalid command: " << str << ".\n";
        else
            cmd->second();
    }

    if (pl_is_opened())
        pl_close();
    pl_item_list_delete(&item_list);

    return 0;
}

template<class T>
static T str2(const string &str) throw (const ios_base::iostate)
{
    T converted;
    stringstream ss;
    ss << str;
    ss >> converted;

    if (ss.rdstate() != istream::eofbit)
        throw ss.rdstate();

    return converted;
}

template<class T>
static T str2(const string &str, string &trailing) throw (const ios_base::iostate)
{
    T converted;
    stringstream ss;
    ss << str;
    ss >> converted;

    if (ss.rdstate() != 0)
        throw ss.rdstate();

    trailing = ss.str();

    return converted;
}

static string trim(const string &input)
{
    string str = input;

    str.erase(0, str.find_first_not_of(" \t"));   // leading spaces
    str.erase(str.find_last_not_of(" \t") + 1);   // trailing spaces

    return str;
}

// Matches "quoted string" or unquoted_string_but_without_spaces.
static string &get_quoted_string(stringstream &ss, string &str)
{
    char c;
    str.clear();

    // Skip leading white spaces.
    ss >> skipws >> c;

    if (c == '\"')
    {
        ss >> noskipws >> c;
        while (c != '\"' && ss.rdstate() == 0)
        {
            str += c;
            ss >> noskipws >> c;
        }
    }
    else
    {
        while (c != ' ' && c != '\t' && ss.rdstate() == 0)
        {
            str += c;
            ss >> noskipws >> c;
        }
    }

    return str;
}

void exit()
{
    running = false;
}

void open()
{
    int retval;
    string argument;

    get_quoted_string(line, argument);

    if (argument.empty())
    {
        cout << "Error: missing database name.\n";
        return;
    }

    if (pl_is_opened())
    {
        cout << "Error: there is already an opened database.\n";
        return;
    }

    dbname = argument;
    retval = pl_open(dbname.c_str());
    if (retval)
    {
        cout << "Error: " << retval << ".\n";
        pl_close();
    }
}

void close()
{
    int retval;

    if (!pl_is_opened())
    {
        cout << "Error: database is not opened.\n";
        return;
    }

    retval = pl_close();
    if (retval)
    {
        cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
    }
    else
    {
        dbname.clear();
        accountname.clear();
        dealloc();
    }
}

void status()
{
    cout << "Database path: " << (pl_is_opened() ? dbname : "(none)") << '\n'

         << "Current account: " << (accountname.empty() ? "(none)" : accountname) << '\n'

         << "Item list: " << item_list.capacity << " allocated, " << item_list.size
         << " used, " << selected.size() << " selected.\n";
}

void account()
{
    string subcommand;
    map<string, Command>::iterator cmd;

    if (!pl_is_opened())
    {
        cout << "Error: database is not opened.\n";
        return;
    }

    line >> skipws >> subcommand;
    cmd = account_commands.find(subcommand);


    if (cmd == account_commands.end())
        cout << "Error: invalid or missing 'account' argument.\n";
    else
        cmd->second();
}

void account_create()
{
    int retval;
    string argument;

    get_quoted_string(line, argument);

    if (argument.empty())
    {
        cout << "Error: missing account name.\n";
        return;
    }

    retval = pl_account_create(argument.c_str());
    if (retval)
        cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
}

void account_delete()
{
    int retval;

    if (accountname.empty())
    {
        cout << "Error: there is no selected account.\n";
        return;
    }

    retval = pl_account_delete(accountname.c_str());
    if (retval)
        cout << "Error: " << retval << ".\n";

    accountname.clear();
}

void account_rename()
{
    int retval;
    string argument;

    get_quoted_string(line, argument);

    if (accountname.empty())
    {
        cout << "Error: there is no selected account.\n";
        return;
    }

    if (argument.empty())
    {
        cout << "Error: missing account new name.\n";
        return;
    }

    retval = pl_account_rename(accountname.c_str(), argument.c_str());
    if (retval)
        cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";

    accountname = argument;
}

void account_list()
{
    int retval;
    char list[512];

    retval = pl_account_list(list, 512, "\n");
    if (retval)
    {
        cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
        return;
    }

    cout << list;
}

void account_set()
{
    string argument;

    get_quoted_string(line, argument);

    if (argument.empty() || pl_account_exists(argument.c_str()))
        accountname = argument;
    else
        cout << "Error: " << argument << " account does not exist.\n";
}

void account_unset()
{
    accountname.clear();
}

static int parse_date(const string &_str, time_t *date)
{
    struct tm bd_time;
    time_t cr_time;

    string str = trim(_str);
    string day, month, year;
    stringstream input;

    cr_time = time(0);
    localtime_r(&cr_time, &bd_time);
    bd_time.tm_hour = 0;
    bd_time.tm_min = 0;
    bd_time.tm_sec = 0;

    if (str == "now" || str == "today")
    {
        cr_time = mktime(&bd_time);
    }
    else if (str == "yesterday")
    {
        bd_time.tm_mday--;
        cr_time = mktime(&bd_time);
    }
    else if (str == "epoch" || str == "begin")
    {
        cr_time = (time_t) 0;
    }
    else if (!str.empty())
    {
        input << str;

        getline(input, day, '-');
        getline(input, month, '-');
        getline(input, year);

        try
        {
            bd_time.tm_mday = str2<int>(day);
            if (!month.empty())
                bd_time.tm_mon = str2<int>(month) - 1;

            if (!year.empty())
                bd_time.tm_year = str2<int>(year) - 1900;

            cr_time = mktime(&bd_time);
        }
        catch (ios_base::iostate &e)
        {
            cr_time = (time_t) -1;
        }
    }
    else
    {
        cr_time = (time_t) -1;
    }


    if (cr_time == (time_t) -1)
    {
        cout << "Error: invalid date.\n";
        return 1;
    }

    *date = cr_time;

    return 0;
}

static string remove_tag(string &str)
{
    size_t found = str.find_first_of(':');
    string tag;

    // there is tag
    if (found != string::npos)
    {
        tag = trim(str.substr(0, found));
        str = trim(str.substr(found + 1));
    }

    return tag;
}

static int parse_description(const string &str, char *tag, char *description)
{
    string sdescription = str, stag;
    size_t length;

    stag = remove_tag(sdescription);

    // there is no tag
    if (stag.empty())
    {
        tag[0] = '\0';
        length = sdescription.copy(description, MAX_DESCRIPTION_SIZE);
        description[length] = '\0';
    }
    // there is tag
    else
    {
        length = stag.copy(tag, MAX_TAG_NAME_SIZE);
        tag[length] = '\0';

        length = sdescription.copy(description, MAX_DESCRIPTION_SIZE);
        description[length] = '\0';
    }

    return 0;
}

static int read_item(pl_item_t *item)
{
    size_t length;

    if (accountname.empty())
    {
        cout << "Error: there is no selected account.\n";
        return 1;
    }

    string date, description, quantity, value;

    get_quoted_string(line, date);
    get_quoted_string(line, description);
    get_quoted_string(line, quantity);
    get_quoted_string(line, value);

    if (date.empty() || description.empty() || quantity.empty())
    {
        cout << "Error: missing tracking arguments.\n";
        return 1;
    }

    if (parse_date(date, &item->date))
        return 1;
    if (parse_description(description, item->tag, item->description))
        return 1;

    // If value is missing, quantity is 1 and value was setted in quantity.
    try
    {
        if (value.empty())
        {
            item->quantity = 1;
            item->value = str2<float>(quantity);
        }
        else
        {
            item->quantity = str2<float>(quantity);
            item->value = str2<float>(value);
        }
    }
    catch (const ios_base::iostate &state)
    {
        cout << "Error: invalid quantity or value.\n";
        return 1;
    }

    length = accountname.copy(item->account, MAX_ACCOUNT_NAME_SIZE);
    item->account[length] = '\0';

    return 0;
}

void report()
{
    map<string, pl_report_t>::iterator r;
    char buffer[MAX_REPORT_SIZE];
    stringstream ss;
    string type;

    // Balance report
    string date_from, date_to;
    time_t from = (time_t) 0;
    time_t to = time(0);
    string account, tag, value;

    if (!pl_is_opened())
    {
        cout << "Error: database is not opened.\n";
        return;
    }

    get_quoted_string(line, type);

    r = reports.find(type);
    if (r == reports.end())
    {
        cout << "Error: invalid or missing report type.\n";
        return;
    }

    switch(r->second)
    {
    case PL_REPORT_BALANCE:
    case PL_REPORT_BALANCE_BY_TAG:
        get_quoted_string(line, date_from);
        get_quoted_string(line, date_to);

        if (!date_from.empty())
            if (parse_date(date_from, &from))
                return;
        if (!date_to.empty())
            if (parse_date(date_to, &to))
                return;

        if (pl_report(buffer, MAX_REPORT_SIZE, "\n", r->second, from, to,
                    accountname.empty() ? NULL : accountname.c_str()))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            return;
        }

        ss << buffer;

        cout.fill(' ');
        cout.width(MAX_ACCOUNT_NAME_SIZE + 1); cout << left << "Account";
        if (r->second == PL_REPORT_BALANCE_BY_TAG)
        {
            cout.width(MAX_TAG_NAME_SIZE + 1);
            cout << left << "Tag";
        }
        cout << "Balance\n";

        getline(ss, account);
        if (r->second == PL_REPORT_BALANCE_BY_TAG)
            getline(ss, tag);
        getline(ss, value);

        while (ss.rdstate() == 0)
        {
            cout.width(MAX_ACCOUNT_NAME_SIZE + 1); cout << left << account;
            if (r->second == PL_REPORT_BALANCE_BY_TAG)
            {
                cout.width(MAX_TAG_NAME_SIZE + 1);
                cout << left << tag;
            }

            cout.width(15); cout.precision(2);
            cout << right << fixed << str2<float>(value) << '\n';

            getline(ss, account);
            if (r->second == PL_REPORT_BALANCE_BY_TAG)
                getline(ss, tag);
            getline(ss, value);
        }

        break;

    default:
        cout << "Error: report not yet implemented.\n";
        return;
    }
}

void income()
{
    pl_item_t item;

    if (!pl_is_opened())
    {
        cout << "Error: database is not opened.\n";
        return;
    }

    if (read_item(&item))
        return;

    if (item.value < 0)
        item.value = - item.value;

    if (pl_tracking_insert(&item))
        cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
}

void spending()
{
    pl_item_t item;

    if (!pl_is_opened())
    {
        cout << "Error: database is not opened.\n";
        return;
    }

    if (read_item(&item))
        return;

    if (item.value > 0)
        item.value = - item.value;

    if (pl_tracking_insert(&item))
        cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
}

void update()
{
    string subcommand;
    map<string, Command>::iterator cmd;

    if (selected.empty())
    {
        cout << "Error: there are no selected itens.\n";
        return;
    }

    line >> skipws >> subcommand;
    cmd = update_commands.find(subcommand);

    if (cmd == update_commands.end())
        cout << "Error: invalid or missing 'update' argument.\n";
    else
        cmd->second();
}

void update_spending()
{
    float bkp;
    set<size_t>::iterator item;

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].value;
        item_list.itens[*item].value = bkp > 0 ? - bkp : bkp;

        if (pl_tracking_update(&item_list.itens[*item]))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            item_list.itens[*item].value = bkp;
        }
    }
}

void update_income()
{
    float bkp;
    set<size_t>::iterator item;

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].value;
        item_list.itens[*item].value = bkp < 0 ? - bkp : bkp;

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            item_list.itens[*item].value = bkp;
        }
    }
}

void update_account()
{
    if (accountname.empty())
    {
        cout << "Error: there is no setted account.\n";
        return;
    }

    string bkp;
    size_t length;
    set<size_t>::iterator item;

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].account;
        length = accountname.copy(item_list.itens[*item].account, MAX_ACCOUNT_NAME_SIZE);
        item_list.itens[*item].account[length] = '\0';

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            length = bkp.copy(item_list.itens[*item].account, MAX_ACCOUNT_NAME_SIZE);
            item_list.itens[*item].account[length] = '\0';
        }
    }
}

void update_date()
{
    string str;
    time_t bkp, date;
    set<size_t>::iterator item;

    get_quoted_string(line, str);
    if (parse_date(str, &date))
        return;

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].date;
        item_list.itens[*item].date = date;

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            item_list.itens[*item].date = bkp;
        }
    }
}

void update_tag()
{
    string bkp, tag;
    size_t length;
    set<size_t>::iterator item;

    get_quoted_string(line, tag);

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].tag;
        length = tag.copy(item_list.itens[*item].tag, MAX_TAG_NAME_SIZE);
        item_list.itens[*item].tag[length] = '\0';

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            length = bkp.copy(item_list.itens[*item].tag, MAX_TAG_NAME_SIZE);
            item_list.itens[*item].tag[length] = '\0';
        }
    }
}

void update_description()
{
    string bkp, description;
    size_t length;
    set<size_t>::iterator item;

    get_quoted_string(line, description);

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].description;
        length = description.copy(item_list.itens[*item].description,
                MAX_DESCRIPTION_SIZE);
        item_list.itens[*item].description[length] = '\0';

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            length = bkp.copy(item_list.itens[*item].description, MAX_DESCRIPTION_SIZE);
            item_list.itens[*item].description[length] = '\0';
        }
    }
}

void update_quantity()
{
    string str;
    float bkp, quantity;
    set<size_t>::iterator item;

    get_quoted_string(line, str);
    quantity = str2<float>(str);

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].quantity;
        item_list.itens[*item].quantity = quantity;

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            item_list.itens[*item].quantity = bkp;
        }
    }
}

void update_value()
{
    string str;
    float bkp, value;
    set<size_t>::iterator item;

    get_quoted_string(line, str);
    value = str2<float>(str);

    for (item = selected.begin(); item != selected.end(); item++)
    {
        bkp = item_list.itens[*item].value;
        item_list.itens[*item].value = value;

        if (pl_tracking_update(item_list.itens + *item))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            item_list.itens[*item].value = bkp;
        }
    }
}

void remove()
{
    set<size_t>::iterator item;
    map<size_t, size_t>::iterator rep;

    if (selected.empty())
    {
        cout << "Error: there are no selected itens.\n";
        return;
    }

    for (item = selected.begin(); item != selected.end(); item++)
    {
        if (pl_tracking_remove(item_list.itens[*item].id))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            continue;
        }

        item_list.itens[*item].id = 0;

        // Remove repeated itens.
        for (rep = repeated.begin(); rep != repeated.end(); rep++)
            if (rep->second == *item)
                item_list.itens[rep->first].id = 0;
    }

    selected.clear();
}

void alloc()
{
    size_t capacity;

    if (item_list.itens)
    {
        cout << "Error: there is already an allocated item list.\n";
        return;
    }

    line >> capacity;
    if ( (line.rdstate() & istream::failbit) || (line.rdstate() & istream::badbit) ||
            capacity < 1 || capacity > 999)
    {
        cout << "Error: invalid or missing item list size.\n";
        return;
    }

    selected.clear();
    pl_item_list_create(&item_list, capacity);
}

void dealloc()
{
    if (item_list.itens)
        pl_item_list_delete(&item_list);
}

static void update_item_list()
{
    size_t i;
    map<int, size_t> ids;
    map<int, size_t>::iterator first;

    if (item_list.size == 0)
        return;

    repeated.clear();

    i = item_list.size;
    do
    {
        i--;

        if (item_list.itens[i].id < 0)
            item_list.itens[i].id = - item_list.itens[i].id;

        first = ids.find(item_list.itens[i].id);
        if (first == ids.end())
        {
            // Item is not repeated.
            ids[item_list.itens[i].id] = i;
            continue;
        }

        // Item is repeated.
        // So, indicate it.
        repeated[i] = first->second;
        item_list.itens[i].id = - item_list.itens[i].id;
        // If it is selected, select only the most recent.
        if (selected.find(i) != selected.end())
        {
            selected.erase(selected.find(i));
            selected.insert(first->second);
        }
    } while (i != 0);
}

void search()
{
    string filter, tag;
    map<string, pl_filter_t>::iterator f;

    // Most recent filter
    unsigned how_many;

    if (!pl_is_opened())
    {
        cout << "Error: database is not opened.\n";
        return;
    }

    if (item_list.size == item_list.capacity)
    {
        cout << "Error: there is no space in item list.\n";
        return;
    }

    get_quoted_string(line, filter);
    tag = remove_tag(filter);

    f = filters.find(filter);
    if (f == filters.end())
    {
        cout << "Error: invalid or missing filter.\n";
        return;
    }

    switch(f->second)
    {
    case PL_FILTER_MOST_RECENT:
        line >> skipws >> how_many;
        if ( line.rdstate() & istream::failbit || line.rdstate() & istream::badbit )
        {
            how_many = item_list.capacity - item_list.size;
            cout << "Warning: using " << how_many
                 << " as argument for 'most recent' filter.\n";
        }
        if (pl_tracking_search(&item_list,
                    accountname.empty() ? NULL : accountname.c_str(),
                    tag.empty() ? NULL : tag.c_str(),
                    PL_FILTER_MOST_RECENT, how_many))
        {
            cout << "Error: " << pl_error(error, MAX_ERROR_MESSAGE_SIZE) << ".\n";
            return;
        }
        break;

    default:
        cout << "Error: filter not yet implemented.\n";
        return;
    }

    update_item_list();
}

void select()
{
    size_t i;
    set<size_t>::iterator it;

    line >> i;
    while ( (line.rdstate() & istream::failbit) == 0 &&
            (line.rdstate() & istream::badbit) == 0)
    {
        if (i < item_list.size && item_list.itens[i].id != 0)
        {
            // Repeated item.
            if (item_list.itens[i].id < 0)
                i = repeated[i];

            it = selected.find(i);
            if (it == selected.end())
                selected.insert(i);
            else
                selected.erase(it);

        }

        if (line.rdstate() & istream::eofbit)
            break;
        line >> i;
    }
}

static void print_centered(const string &str, unsigned wide)
{
    if (wide < str.length())
        return;

    unsigned before = ((unsigned) (wide - str.length())) / 2;
    unsigned after = wide - before;

    cout.width(before); cout << ' ';
    cout.width(after); cout << left << str;
}

void display()
{
    float sum = 0.0f, ssum = 0.0f;
    struct tm bd_time;
    set<size_t>::iterator next_selected;
    pl_item_t *item;

    next_selected = selected.begin();

    cout.fill(' ');
    cout << "       ";
    print_centered("Date", 11);
    print_centered("Account", MAX_ACCOUNT_NAME_SIZE + 2);
    print_centered("Description", MAX_TAG_NAME_SIZE + MAX_DESCRIPTION_SIZE + 2);
    cout << "Quantity";
    print_centered("Value", 15);
    print_centered("Total", 15);
    cout << '\n';

    for (size_t i = 0; i < item_list.size; i++)
    {
        cout.fill('0');
        cout.width(3);
        cout << right << i << ' ';

        if (item_list.itens[i].id == 0)
        {
            cout << "[-] ...\n";
            continue;
        }

        if (item_list.itens[i].id < 0)
        {
            cout << "[?] -> " << repeated.at(i) << '\n';
            continue;
        }

        item = item_list.itens + i;
        localtime_r(&item->date, &bd_time);

        if (next_selected != selected.end() && *next_selected == i)
        {
            cout << "[*] ";
            next_selected++;

            ssum += item->quantity * item->value;
        }
        else
            cout << "[ ] ";

        sum += item->quantity * item->value;

        // Day
        cout.width(2);
        cout << right << bd_time.tm_mday << '-';
        // Month
        cout.width(2);
        cout << right << (bd_time.tm_mon + 1) << '-';
        // Year
        cout.width(4);
        cout << (bd_time.tm_year + 1900) << ' ';

        cout.fill(' ');

        cout.width(MAX_ACCOUNT_NAME_SIZE + 1);
        cout << left << item->account;

        cout.width(MAX_TAG_NAME_SIZE);
        if (item->tag[0] == '\0')
            cout << ' ' << ' ';
        else
            cout << right << item->tag << ':';

        cout.width(MAX_DESCRIPTION_SIZE + 1);
        cout << left << item->description;

        cout.unsetf(ios_base::floatfield);
        cout.precision(3), cout.width(9);
        cout << right << item->quantity;

        cout.precision(2); cout.width(12);
        cout << right << fixed << (item->value > 0 ? item->value : -item->value);

        cout << (item->value > 0 ? "(+) " : "(-) ");

        cout.precision(2); cout.width(12);
        cout << right << fixed << (item->quantity * (item->value > 0 ? item->value : -item->value));

        cout << (item->value > 0 ? "(+) " : "(-) ");
        cout << '\n';
    }
    cout << "sum: " << sum;
    if (!selected.empty())
        cout << '(' << ssum << ')';
    cout << ", mean: " << (sum / item_list.size);
    if (!selected.empty())
        cout << '(' << (ssum / selected.size()) << ')';
    cout << '\n';
}

// Print help command
static void phcmd(const char *command, const char *alias, const char *description)
{
    cout.fill(' ');
    cout.width(25);
    cout << left << command;
    cout.width(6);
    cout << left << alias
         << " -- " << description << ".\n";
}

void help()
{
    cout << "General commands\n";
    phcmd("status",             "st",   "show current session informations");
    phcmd("exit",               "x",    "exit pluto");
    phcmd("help",               "h",    "show this message");

    cout << "\nDatabase manipulation commands\n";
    phcmd("open FILENAME",      "o",    "open/create database in FILENAME");
    phcmd("close",              "c",    "close current opened database");

    cout << "\nAccount manipulation commands\n";
    phcmd("account create NAME","ac c", "create NAME named account");
    phcmd("account delete",     "ac d", "delete current setted account");
    phcmd("account rename NAME","ac r", "rename current setted account to NAME");
    phcmd("account set NAME",   "ac s", "set NAME named account as current");
    phcmd("account unset",      "ac u", "unset current account");
    phcmd("account list",       "ac l", "show all account names");

    cout << "\nTracking commands\n";
    phcmd("report REPORT [OPT]","r",    "show REPORT report");
    phcmd("income ITEM",        "in",   "track ITEM as an income");
    phcmd("spending ITEM",      "sp",   "track ITEM as a spending");

    cout << "\nItem list manipulation commands\n";
    phcmd("allocate NUMBER",    "alloc","allocate NUMBER sized item list");
    phcmd("deallocate",         "de",   "deallocate the current item list");
    phcmd("search FILTER [OPT]","s",    "search based on FILTER");
    phcmd("display",            "d",    "show item list");
    phcmd("select NUMBER...",   "sel",  "select itens");
    phcmd("remove",             "rem",  "remove selected itens");
    // TODO update
    // TODO explicar NUMBER, FILTER, OPT, NAME...
}
